﻿using System;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Reclama5.Domain;
using Reclama5.Domain.Entities;
using Reclama5.Service;


namespace Reclama5.Areas.Admin.Controllers 
{
    [Area("Admin")]
    public class TextFieldsController : Controller
    {
        //внедряем датаменеджер чтобы иметь доступ к база данных
        private readonly DataManager dataManager;
        private readonly IWebHostEnvironment hostingEnvironment;
        public TextFieldsController(DataManager dataManager, IWebHostEnvironment hostingEnvironment)
        {
            this.dataManager = dataManager;
            this.hostingEnvironment = hostingEnvironment;
        }

        public IActionResult Edit(string codeWord)
        {
            var entity = dataManager.TextFields.GetTextFieldByCodeWord(codeWord);
            return View(entity);
        }
        [HttpPost]
        public IActionResult Edit(TextField model)
        {
            if (ModelState.IsValid)
            {
                dataManager.TextFields.SaveTextField(model);
                return RedirectToAction(nameof(HomeController.Index), nameof(HomeController).CutController());
            }
            return View(model);
        }
    }
}
