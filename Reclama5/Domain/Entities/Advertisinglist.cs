﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Reclama5.Domain.Entities
{
    public class Advertisinglist : EntityBase
    {
        [Display(Name = "Полное описание торгов")]
        public override string? Text { get; set; }

        [Display(Name = "Порядковый номер конструкции")]
        public override string? Number { get; set; }

        [Display(Name = "Вид рекламной конструкции")]
        public override string? Type { get; set; }

        [Display(Name = "Площадь")]
        public override string? Square { get; set; }

        [Display(Name = "Адрес")]
        public override string? Address { get; set; }

        [Display(Name = "Рекламное поля А")]
        public override string? FieldA { get; set; }

        [Display(Name = "Рекламное поля Б")]
        public override string? FieldB { get; set; }

        [Required(ErrorMessage = "Заполните вид торгов")]
        [Display(Name = "Вид констр")]
        public override string? Title { get; set; }

        [Display(Name = "Краткое описание констр")]
        public override string? Subtitle { get; set; }
    }
}
