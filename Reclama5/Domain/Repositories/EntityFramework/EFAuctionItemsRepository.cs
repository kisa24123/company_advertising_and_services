﻿using Reclama5.Domain.Entities;
using Reclama5.Domain.Repositories.Abstract;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Reclama5.Domain.Repositories;

 

namespace Reclama5.Domain.Repositories.EntityFramework
{
    public class EFAuctionItemsRepository : IAuctionItemsRepository
    {
        private readonly AppDbContext context;
        // внедрение зависимостей
        public EFAuctionItemsRepository(AppDbContext context)
        {
            this.context = context;
        }

        public IQueryable<AuctionItem> GetAuctionItems()
        {
            //вытягиваем из таблицы
            return context.AuctionItems;
        }

        public AuctionItem GetAuctionItemById(Guid id)
        {
            return context.AuctionItems.FirstOrDefault(x => x.Id == id);
        }

        public void SaveAuctionItem(AuctionItem entity)
        {

            if (entity.Id == default)
                //добавле
                context.Entry(entity).State = EntityState.Added;
            else
                // указываем, что объект существует entity уже в базе данных, и для него надо внести в базу измененное значение, а не создавать новую запись
                context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();
            
        }

        public void DeleteAuctionItem(Guid id)
        {
            context.AuctionItems.Remove(new AuctionItem() { Id = id });
            context.SaveChanges();
        }

    }
}
