﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Reclama5.Domain.Entities
{ 
    public abstract class EntityBase
    {
        protected EntityBase() => DateAdded = DateTime.UtcNow;

    
        [Required]
        public Guid Id { get; set; }

        [Display(Name = "Название (заголовок)")] 
        public virtual string? Title { get; set; } 

        [Display(Name = "Краткое описание")]
        public virtual string? Subtitle { get; set; } 

        [Display(Name = "Полное описание")]
        public virtual string? Text { get; set; } 

        [Display(Name = "Титульная картинка")]
        public virtual string? TitleImagePath { get; set; } 

        [Display(Name = "SEO метатег Title")]
        public string? MetaTitle { get; set; } 

        [Display(Name = "SEO метатег Description")]
        public string? MetaDescription { get; set; }

        [Display(Name = "SEO метатег Keywords")]
        public string? MetaKeywords { get; set; } 

        [DataType(DataType.Time)]
        public DateTime DateAdded { get; set; }

        [Display(Name = "Порядковый номер")]
        public virtual string? Number { get; set; }

        [Display(Name = "Вид рекламной конструкции")]
        public virtual string? Type { get; set; }

        [Display(Name = "Площадь")]
        public virtual string? Square { get; set; }

        [Display(Name = "Адрес")]
        public virtual string? Address { get; set; }

        [Display(Name = "Рекламное поля А")]
        public virtual string? FieldA{ get; set; }

        [Display(Name = "Рекламное поля Б")]
        public virtual string? FieldB { get; set; }






    }
}
