﻿namespace Reclama5.Service
{

    public static class Extensions
    {
        //при входе строки вырезаем слово контроллер
        public static string CutController(this string str)
        {
            return str.Replace("Controller", "");
        }
    }
}
