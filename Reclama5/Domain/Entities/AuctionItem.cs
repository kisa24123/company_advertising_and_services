﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Reclama5.Domain.Entities
{
    // услуга на сайте
    public class AuctionItem : EntityBase
    {
        [Required(ErrorMessage = "Заполните вид торгов")]
        [Display(Name = "Вид торгов")]
        public override string? Title { get; set; }

        [Display(Name = "Краткое описание торгов")]
        public override string? Subtitle { get; set; }

        [Display(Name = "Полное описание торгов")]
        public override string? Text { get; set; }
       

    }
}
