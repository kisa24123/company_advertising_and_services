﻿using Microsoft.AspNetCore.Mvc;
using Reclama5.Domain;

namespace Reclama5.Controllers
{
    public class AdvertisingsController : Controller
    {
        private readonly DataManager dataManager;

        public AdvertisingsController(DataManager dataManager)
        {
            this.dataManager = dataManager;
        }

        public IActionResult Index(Guid id)
        {
            if (id != default)
            {
                // конкретная конструкция 
                return View("Show", dataManager.Advertisinglists.GetAdvertisinglistById(id));
            }
            // если id равен дефолдному значению то показывет список всех конструкций
            ViewBag.TextField = dataManager.TextFields.GetTextFieldByCodeWord("PageAdvertisings");
            return View(dataManager.Advertisinglists.GetAdvertisinglists());
        }
    }
}
