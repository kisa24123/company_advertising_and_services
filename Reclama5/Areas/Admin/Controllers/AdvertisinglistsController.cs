﻿using System;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Reclama5.Domain;
using Reclama5.Domain.Entities;
using Reclama5.Service;
namespace Reclama5.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class AdvertisinglistsController : Controller
    {
        private readonly DataManager dataManager;
        // для сохранения титульных картинок
        private readonly IWebHostEnvironment hostingEnvironment;
        public AdvertisinglistsController(DataManager dataManager, IWebHostEnvironment hostingEnvironment)
        {
            this.dataManager = dataManager;
            this.hostingEnvironment = hostingEnvironment;
        }

        public IActionResult Edit(Guid id)
        {
            // по id ищем услугу в базн днных если конструкция не найдена то создаем новый объект класса если найдена то выбираем из базы данных
            var entity = id == default ? new Advertisinglist() : dataManager.Advertisinglists.GetAdvertisinglistById(id);
            return View(entity);
        }
        [HttpPost]
        public IActionResult Edit(Advertisinglist model, IFormFile titleImageFile)
        {
            if (ModelState.IsValid)
            {
                if (titleImageFile != null)
                {
                    // название картинки сохраняем в свойство
                    model.TitleImagePath = titleImageFile.FileName;

                    //Для получения полного пути к каталогу wwwroot применяется свойство WebRootPath объекта IWebHostEnvironment. Для копирования файла в папку  images создается поток FileStream, в который записывается файл с помощью метода CopyToAsync
                    using (var stream = new FileStream(Path.Combine(hostingEnvironment.WebRootPath, "images/", titleImageFile.FileName), FileMode.Create))
                    {
                        titleImageFile.CopyTo(stream);
                    }
                }
                dataManager.Advertisinglists.SaveAdvertisinglist(model);
                return RedirectToAction(nameof(HomeController.Index), nameof(HomeController).CutController());
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(Guid id)
        {
            dataManager.Advertisinglists.DeleteAdvertisinglist(id);
            return RedirectToAction(nameof(HomeController.Index), nameof(HomeController).CutController());
        }
    }
}
