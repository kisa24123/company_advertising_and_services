﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Reclama5.Domain.Entities;

namespace Reclama5.Domain.Repositories.Abstract
{
    // реализуют нужное нам поведение (на страницы будут кнопки с помощью которых можно редактировать)
    public interface IAuctionItemsRepository
    {
        // сделать выборку всех услуг
        IQueryable<AuctionItem> GetAuctionItems();

        //провалиться в какую небудь услугу
        AuctionItem GetAuctionItemById(Guid id);

        // сохранить изменения или обновить
        void SaveAuctionItem(AuctionItem entity);

        // удалить 
        void DeleteAuctionItem(Guid id);

    }
}
