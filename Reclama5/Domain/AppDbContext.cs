﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Reclama5.Domain.Entities;

namespace Reclama5.Domain.Repositories
{
    public class AppDbContext : IdentityDbContext<IdentityUser>


    {
        //Через параметр options в конструктор контекста данных будут передаваться настройки контекста. (например создается база данных)
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        //IdentityDbContext<IdentityUser>
        // Через это свойство будет осуществляться связь с таблицей, где будут храниться данные объектов
        public DbSet<TextField> TextFields { get; set; }
        public DbSet<AuctionItem> AuctionItems { get; set; }
        public DbSet<Advertisinglist> Advertisinglists { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // использование Fluent API
            base.OnModelCreating(modelBuilder);

            //включение сущности в модель представляет вызов Entity() объекта ModelBuilder в методе OnModelCreating():
            //Для инициализации БД при конфигурации определенной модели вызывается метод HasData(), IdentityRole

            modelBuilder.Entity<IdentityRole>().HasData(new IdentityRole
            {
                Id = "44546e06-8719-4ad8-b88a-f271ae9d6eab",
                Name = "admin",
                NormalizedName = "ADMIN"
            });

            modelBuilder.Entity<IdentityUser>().HasData(new IdentityUser
            {
                Id = "3b62472e-4f66-49fa-a20f-e7685b9565d8",
                UserName = "admin",
                NormalizedUserName = "ADMIN",
                Email = "my@email.com",
                NormalizedEmail = "MY@EMAIL.COM",
                EmailConfirmed = true,
                PasswordHash = new PasswordHasher<IdentityUser>().HashPassword(null, "superpassword"),
                SecurityStamp = string.Empty
            });

            modelBuilder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string>
            {
                RoleId = "44546e06-8719-4ad8-b88a-f271ae9d6eab",
                UserId = "3b62472e-4f66-49fa-a20f-e7685b9565d8"
            });

            modelBuilder.Entity<TextField>().HasData(new TextField
            {
                Id = new Guid("63dc8fa6-07ae-4391-8916-e057f71239ce"),
                CodeWord = "PageIndex",
                Title = "Главная"
            });
            modelBuilder.Entity<TextField>().HasData(new TextField
            {
                Id = new Guid("70bf165a-700a-4156-91c0-e83fce0a277f"),
                CodeWord = "PageAuctions",
                Title = "Торги"
            });

            modelBuilder.Entity<TextField>().HasData(new TextField
            {
                Id = new Guid("921080c5-25d2-4098-9dce-ed1dfe2ac62f"),
                CodeWord = "PageAdvertisings",
                Title = "Список конструкций"
            });
            modelBuilder.Entity<TextField>().HasData(new TextField
            {
                Id = new Guid("4aa76a4c-c59d-409a-84c1-06e6487a137a"),
                CodeWord = "PageContacts",
                Title = "Контакты"
            });
            modelBuilder.Entity<TextField>().HasData(new TextField
            {
                Id = new Guid("84a5291a-e1c3-467d-bb39-f0f4eebd00e9"),
                CodeWord = "PageManagement",
                Title = "Руководство"
            });
            modelBuilder.Entity<TextField>().HasData(new TextField
            {
                Id = new Guid("142a6e06-9e4d-4dc7-98a0-6adf4bc9bd6a"),
                CodeWord = "PageAbout",
                Title = "О предприятии"
            });
        }
    }
}


