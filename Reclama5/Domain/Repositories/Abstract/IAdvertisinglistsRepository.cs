﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Reclama5.Domain.Entities;

namespace Reclama5.Domain.Repositories.Abstract
{
    public interface IAdvertisinglistsRepository
    {
        // сделать выборку всех услуг
        IQueryable<Advertisinglist> GetAdvertisinglists();

        //провалиться в какую небудь услугу
        Advertisinglist GetAdvertisinglistById(Guid id);

        // сохранить изменения или обновитьf
        void SaveAdvertisinglist(Advertisinglist entity);

        // удалить 
        void DeleteAdvertisinglist(Guid id);
    }
}
