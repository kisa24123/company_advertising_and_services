# company_Advertising_and_services



## Tools and technologies


1. Before developing the site, he visualized the design layout of the site using graphic editors (**Photoshop**, **CorelDRAW**).
2. The web application was created using **ASP.NET Core MVC** technologies.
3. During the development process, I used the **Visual Studio** development environment, **MS SQL Server**.
4. I used **C#** as the main programming language for the back-end.
5. **Entity Framework** is used as the ORM platform.
6. **Identity** technology was used to connect the user identification system (authentication and authorization).
7. An admin panel has been created, access to which is not available to all users.Through the admin panel (login: admin, password: superpassword) you can create, edit and delete content on the site.
8. Connected the CKEditor editor, with which the site administrator can manage the content on the site.
9. Developed and maintained web applications using **HTML-5**, **CSS-3 (SCSS)**, **JavaScript (ES5,ES6)**. When creating a site, Flex, Grid technologies are used. Responsive web design. semantic design. Responsive web design. Using microdata to create a website. Used **jQuer** to improve the look of the web page.



