﻿using Microsoft.AspNetCore.Mvc;
using Reclama5.Domain;

namespace Reclama5.Controllers
{
    public class AuctionsController : Controller
    {
        private readonly DataManager dataManager;

        public AuctionsController(DataManager dataManager)
        {
            this.dataManager = dataManager;
        }
        // 
        public IActionResult Index(Guid id)
        {
            if (id != default)
            {
                // конкретная аукцион
                return View("Show", dataManager.AuctionItems.GetAuctionItemById(id));
            }
            // если id равен дефолдному значению то показывет список всех аукционов
            ViewBag.TextField = dataManager.TextFields.GetTextFieldByCodeWord("PageAuctions");
            return View(dataManager.AuctionItems.GetAuctionItems());
        }
    }
}
