﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Reclama5.Domain.Repositories.Abstract;

namespace Reclama5.Domain
{
    public class DataManager
    {
        public ITextFieldsRepository TextFields { get; set; }
        public IAuctionItemsRepository AuctionItems { get; set; }
        public IAdvertisinglistsRepository Advertisinglists { get; set; }

        public DataManager(ITextFieldsRepository textFieldsRepository, IAuctionItemsRepository auctionItemsRepository, IAdvertisinglistsRepository advertisinglistsRepository )
        {
            TextFields = textFieldsRepository;
            AuctionItems = auctionItemsRepository;
            Advertisinglists = advertisinglistsRepository;
        }
    }
}
