﻿using Reclama5.Domain.Entities;
using Reclama5.Domain.Repositories.Abstract;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Reclama5.Domain.Repositories;

namespace Reclama5.Domain.Repositories.EntityFramework
{
    public class EFAdvertisinglistsRepository : IAdvertisinglistsRepository
    {
        private readonly AppDbContext context;

        public EFAdvertisinglistsRepository(AppDbContext context)
        {
            this.context = context;
        }

        public IQueryable<Advertisinglist> GetAdvertisinglists()
        {
            //вытягиваем из таблицы
            return context.Advertisinglists;
        }

        public Advertisinglist GetAdvertisinglistById(Guid id)
        {
            return context.Advertisinglists.FirstOrDefault(x => x.Id == id);
        }

        public void SaveAdvertisinglist(Advertisinglist entity)
        {

            if (entity.Id == default)
                //добавле
                context.Entry(entity).State = EntityState.Added;
            else
                // указываем, что объект существует entity уже в базе данных, и для него надо внести в базу измененное значение, а не создавать новую запись
                context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();

        }

        public void DeleteAdvertisinglist(Guid id)
        {
            context.Advertisinglists.Remove(new Advertisinglist() { Id = id });
            context.SaveChanges();
        }

    }
}
