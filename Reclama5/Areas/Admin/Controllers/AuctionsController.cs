﻿using System;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Reclama5.Domain;
using Reclama5.Domain.Entities;
using Reclama5.Service;


namespace Reclama5.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class AuctionsController : Controller
    {

        private readonly DataManager dataManager;

        public AuctionsController(DataManager dataManager)
        {
            this.dataManager = dataManager;
        }

        public IActionResult Index()
        {
            return View(dataManager.AuctionItems.GetAuctionItems());
        }

    }
}
