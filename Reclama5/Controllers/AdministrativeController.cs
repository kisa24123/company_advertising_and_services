﻿using Microsoft.AspNetCore.Mvc;

namespace Reclama5.Controllers
{
    public class AdministrativeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Extend()
        {
            return View();
        }
        public IActionResult Reregister()
        {
            return View();
        }
        public IActionResult Duplicate()
        {
            return View();
        }
    }
}
