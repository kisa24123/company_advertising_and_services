﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Reclama5.Domain.Entities;

namespace Reclama5.Domain.Repositories.Abstract
{
    public interface ITextFieldsRepository
    {
        // сделать выборку всех тестовых полей
        IQueryable<TextField> GetTextFields();

        // выбрать тектовое поле по id
        TextField GetTextFieldById(Guid id);

        // выбрать тектовое поле по ключевому слов
        TextField GetTextFieldByCodeWord(string codeWord);

        // сохранить изменения 
        void SaveTextField(TextField entity);

        // удалить 
        void DeleteTextField(Guid id);

    }
}
