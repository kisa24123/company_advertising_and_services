﻿using Microsoft.AspNetCore.Mvc;
using Reclama5.Domain;

namespace Reclama5.Controllers
{
    public class HomeController : Controller
    {
        private readonly DataManager dataManager;

        public HomeController(DataManager dataManager)
        {
            this.dataManager = dataManager;
        }

        public IActionResult Index()
        {
            return View(dataManager.TextFields.GetTextFieldByCodeWord("PageIndex"));
        }

        public IActionResult Contacts()
        {
            return View(dataManager.TextFields.GetTextFieldByCodeWord("PageContacts"));
        }

        public IActionResult Management()
        {
            return View(dataManager.TextFields.GetTextFieldByCodeWord("PageManagement"));
        }

        public IActionResult About()
        {
            return View(dataManager.TextFields.GetTextFieldByCodeWord("PageAbout"));
        }
    }
}
