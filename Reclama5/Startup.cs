﻿using Reclama5.Service;
using Reclama5.Domain.Repositories.EntityFramework;
using Reclama5.Domain.Repositories.Abstract;
using Reclama5.Domain.Repositories;
using Reclama5.Domain;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Reclama5
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration) => Configuration = configuration;

        public void ConfigureServices(IServiceCollection services)
        {
            //подключаем конфиг из appsetting.json
            Configuration.Bind("Project", new Config());

            //подключаем нужный функционал приложения в качестве сервисов (Регистрируем сервисы)
            services.AddTransient<ITextFieldsRepository, EFTextFieldsRepository>();
            services.AddTransient<IAuctionItemsRepository, EFAuctionItemsRepository>();
            services.AddTransient<IAdvertisinglistsRepository, EFAdvertisinglistsRepository>();
            services.AddTransient<DataManager>();

          
            // добавляем контекст AppDbContext в качестве сервиса в приложение
            services.AddDbContext<AppDbContext>(x => x.UseSqlServer("Server = (localdb)\\mssqllocaldb; Database = Reclama5; Persist Security Info = false; User ID = 'sa'; Password = 'sa'; Trusted_Connection = True;"));

            //настраиваем identity систему
            services.AddIdentity<IdentityUser, IdentityRole>(opts =>
            {
                opts.User.RequireUniqueEmail = true;
                opts.Password.RequiredLength = 6;
                opts.Password.RequireNonAlphanumeric = false;
                opts.Password.RequireLowercase = false;
                opts.Password.RequireUppercase = false;
                opts.Password.RequireDigit = false;
            }).AddEntityFrameworkStores<AppDbContext>().AddDefaultTokenProviders();

            //настраиваем authentication cookie
            services.ConfigureApplicationCookie(options =>
            {
                options.Cookie.Name = "myCompanyAuth";
                options.Cookie.HttpOnly = true;
                // все не авторизованые и не аутентиф. пользователи отпр по этому адресу
                options.LoginPath = "/account/login";
                options.AccessDeniedPath = "/account/accessdenied";
                options.SlidingExpiration = true;
            });

            //настраиваем политику авторизации для Admin area
            services.AddAuthorization(x =>
            {
                //добавляет политику
                //Первый параметр метода представляет название политики
                //второй - делегат,  позволяет создать политику по определенным условиям
                //   RequireRole(roles): пользователь должен принадлежать к одной из ролей из массива roles

                x.AddPolicy("AdminArea", policy => { policy.RequireRole("admin"); });
            });

            //добавляем сервисы для контроллеров и представлений (MVC)
            services.AddControllersWithViews(x =>
            {
                x.Conventions.Add(new AdminAreaAuthorization("Admin", "AdminArea"));
            })
                //выставляем совместимость с asp.net core 3.0
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0).AddSessionStateTempDataProvider();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            

            //в процессе разработки нам важно видеть какие именно ошибки
            //EnvironmentName: хранит название среды, в которой хостируется приложение
            //IsDevelopment(): возвращает true, если имя среды - Development
            // если приложение находится на стадии разработкки то при ошибке разработчик увидит детальное описание ошибки

            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            //подключаем поддержку статичных файлов в приложении (css, js и т.д.)
            app.UseStaticFiles();

            //подключаем систему маршрутизации
            app.UseRouting();

            //подключаем аутентификацию и авторизацию
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseAuthorization();

            //регистриуруем нужные нам маршруты (ендпоинты)
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute("admin", "{area:exists}/{controller=Home}/{action=Index}/{id?}");
                endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
