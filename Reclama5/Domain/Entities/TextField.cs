﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Reclama5.Domain.Entities
{ 
    // содержание например о компании
    public class TextField : EntityBase
    {
        // ключевое слово по которому сможем обращаться
        [Required]
        public string? CodeWord { get; set; } 

        [Display(Name = "Название страницы (заголовок)")]
        public override string? Title { get; set; } 

        [Display(Name = "Cодержание страницы")]
        public override string? Text { get; set; } 
    }


}

